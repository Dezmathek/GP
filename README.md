* [**UC Browser**](http://www.ucweb.com/ucbrowser/) | [**Google Chrome**](https://www.google.com/chrome/)

	- [uBlock Origin](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm?hl=en-US)
	- [HTTPS Everywhere](https://chrome.google.com/webstore/detail/https-everywhere/gcbommkclmclpchllfjekcdonpmejbdp?hl=en)
	- [Flashcontrol](https://chrome.google.com/webstore/detail/flashcontrol/mfidmkgnfgnkihnjeklbekckimkipmoe?hl=en-US)
	- [UpNext Music Player](https://chrome.google.com/webstore/detail/upnext-music-player/dgkfcdlmdppfhbfmooinbcejdaplobpk?hl=en-US)
	- [Download Master](https://chrome.google.com/webstore/detail/download-master/dljdacfojgikogldjffnkdcielnklkce?hl=en-US)

------------------------------------------------------------------------

* [**Mozilla Firefox**](https://www.mozilla.org/)<br>

	- [uBlock Origin ](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/?src=search)
	- [HTTPS Everywhere](https://addons.mozilla.org/en-us/firefox/addon/https-everywhere/)
 	- [Список аддонов и рекомендаций](https://github.com/The-OP/Fox)

 	- [Stylish](https://addons.mozilla.org/en-US/firefox/addon/stylish/)
		+ [Combined favicon and close button](https://userstyles.org/styles/116925/firefox-combined-favicon-and-close-button)
		+ [Microsoft Edge style square tabs](https://userstyles.org/styles/117846/microsoft-edge-style-square-tabs)
		+ [minimal floating scrollbars for firefox windows](https://userstyles.org/styles/83431/minimal-floating-scrollbars-for-firefox-windows)

------------------------------------------------------------------------

* **Media Player:**

	- [MPC-BE](http://sourceforge.net/projects/mpcbe/)

------------------------------------------------------------------------

* **Cloud Storage:**

	- [Google drive](https://www.google.com/intl/en/drive)
	- [Dropbox](https://www.dropbox.com)

------------------------------------------------------------------------

* **Torrent-client:**

	- [BitTorrent](http://www.bittorrent.com/)

------------------------------------------------------------------------

* **Audio Players:**

	+ [AIMP3](http://www.aimp.ru/)
		+ [Metro v1.6](http://www.aimp.ru/index.php?do=catalog&rec_id=408)

------------------------------------------------------------------------

* **Code Editors:**

	- [Notepad++](http://notepad-plus-plus.org/)
	- [Atom](https://atom.io/)

------------------------------------------------------------------------

* **Communication apps:**

	- [Skype](http://www.skype.com/)
	- [Telegram](https://telegram.org/)

------------------------------------------------------------------------

* **File Archivers:**

	- [7-Zip](http://www.7-zip.org/)

------------------------------------------------------------------------

* **Остальное:**

	- [Git](https://git-scm.com/)
	- [TortoiseGit](https://tortoisegit.org/)
	- [Download Master](https://westbyte.com/dm/index.phtml?page=download)
	- [Volume2](https://irzyxa.wordpress.com/)
	- [Lightscreen](http://lightscreen.com.ar/)
